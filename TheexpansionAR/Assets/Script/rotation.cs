﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation : MonoBehaviour
{
    public float yRotate , time;
    public bool Isrotate = false; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Isrotate == true)
        {
            time += Time.deltaTime; 
            if(time > 1.5)
            {
                Rotation();
            }
        }
    }
    public void OnEnable()
    {
        Isrotate = true; 
    }
    void OnDisable()
    {
        time = 0;
        this.transform.localEulerAngles = new Vector3(0, 180, 0); 
    }
    void Rotation()
    {
        //this.transform.localEulerAngles += new Vector3(0, yRotate, 0); 
        this.transform.localEulerAngles = new Vector3(0,this.transform.localEulerAngles.y, 0); 
        this.transform.Rotate(0f,yRotate,0f,Space.Self);
    }
}
